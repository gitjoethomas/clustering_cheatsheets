import pandas
from datetime import datetime, timedelta

from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style = "whitegrid")

def monetary(dataframe):
    
    "calculates monetary value of each customer, using a group_by()"
    
    spend = dataframe.groupby("customerid")['spend'].sum() # returns series
    spend = spend.reset_index() # groupby() sets your grouper to the index, so we need to reset it
    
    return spend

def frequency(dataframe):
    
    "calculates number of transactions each customer has made"
    
    visits = dataframe.groupby("customerid")['spend'].count() # we could have counted invoicedate, it doesn't matter
    visits = visits.reset_index()
    visits = visits.rename(columns = {"spend": "visits"})
    
    return visits

def recency(dataframe, date_today):
    
    """
    calculates how long ago each customer's most recent transaction was. Returns a household_level df, showing the number
    of days between the customer's most recent transaction, and the date you supplied. 'date_today' must be a datetime object
    """
    
    recency = dataframe.groupby("customerid")["invoicedate"].max() # latest transaction date
    recency = recency.reset_index()
    recency = recency.rename(columns = {"invoicedate":"most_recent_transaction"})
    
    # how many days have passed since their most recent transaction?
    recency['num_days_since_last_tran'] = date_today - recency['most_recent_transaction']
    
    # take off the 'days' bit - we're only interested in the number
    recency['num_days_since_last_tran'] = recency['num_days_since_last_tran'].astype(str)
    recency['num_days_since_last_tran'] = recency['num_days_since_last_tran'].str[:-23]
    recency['num_days_since_last_tran'] = recency['num_days_since_last_tran'].astype(int)
    
    return recency[['customerid', 'num_days_since_last_tran']]

def visualise(dataframe, title, file_name):
    
    fig, ax = plt.subplots(figsize = (10, 10))
    
    ax.set_title(f"{title}", size = 16, pad = 15)
    ax.set_xlabel(dataframe.columns[0], size = 14)
    ax.set_ylabel(dataframe.columns[1], size = 14)

    # columns 1 and 2 are the x and y axes, column 3 determines colour
    ax.scatter(dataframe.iloc[:,0], dataframe.iloc[:,1], c = dataframe.iloc[:,2]) 
    
    plt.savefig(f'images/{file_name}')
    plt.show()