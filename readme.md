# Summary of different clustering methodologies

##### Note - the performance metrics in this readme are out of date, as I changed the dataset I was clustering. Will fix.

### Summary

When entering the world of machine learning and clustering it's easy to fall into the trap of using KMeans and being satisfied with the result. It's a robust and widely-applicable model, and its methodology can be fairly easily explained to stakeholders. 

However, there are many more models out there and you might find with a bit of exploration you can get a much better-suited model for your data. 

This is an exploration of several different models, and when they are/aren't suitable.

Models:

* Kmeans
* Hierarchical/Agglomerative
* Birch
* Affinity Propogation

What are we modelling? I took a retail sales dataset from kaggle (https://www.kaggle.com/jihyeseo/online-retail-data-set-from-uci-ml-repo) and have generated RFM (recency, frequency, monetary) scores. We will be clustering customers based on these scores.

Let's have a look at the distributions for these features...

![Feature Distributions](images/distributions.png)

These features aren't on the same scale. This is a problem for a lot of clustering models, because they think in terms of 'euclidean distance' ('euclidean' means 'as the crow flies' - the direct distance between two datapoints). If your features are on different scales, then they can't be compared. We will convert features to the same scale when needed (more on this later).

```python
scaler = StandardScaler()
scaled_data = scaler.fit_transform(df)
scaled = pd.DataFrame(scaled_data, df.index, df.columns)
```

### Summary

Here's an overview of the algorithms and when they're good and bad:

Model   |   Size of Dataset |   Clusters    |   Notes
------- | ----------------- | ------------- | ----------
Kmeans  |      Very big     |    You choose | Very general-purpose
Kmodes  |      Very big     |    You choose | Kmeans but for all-boolean data
Affinity Propagation | Medium | Determined for you | Hard to predict whether it'll be fast or slow
Agglomerative | Small/medium| You make informed decision| Very intensive in large datasets
Birch   |       Very big    |
DBScan  |   Small/Medium    |   Determined for you  |   Resistant to outliers


### KMeans

kmeans is the most common clustering model, and should probably be run as a 'target to beat' when running a clustering project

when instantiating, you tell the model how many clusters (what it calls 'centroids') there should be

the model will place all centroids semi-randomly in a copy of your data. The steps are then as follows:

1. label each datapoint with the centroid it belongs to (i.e. the nearest centroid)
2. move each centroid so that it is in the middle of all its datapoints
3. repeat steps 1 and 2 until there is no further movement (this is called convergence)

``` python
# the most important argument is n_clusters - it is how you set how many clusters you want.
kmeans = KMeans(n_clusters=6,
                init='k-means++', # the model will use intelligent placement of centroids to reach convergence faster
                n_init=10, # model runs this many times, the iteration which had the smallest datapoint-reassignments is used
                max_iter=300, # how many reassignments will we do in a run before giving up if we haven't reached convergence?
                tol=0.0001, # a centroid moving less than this is considered to not have moved
                precompute_distances='auto', # True is faster but uses more memory
                verbose=0, # True or False - do you want the model to print diagnostics as it goes?
                random_state=42, # this process is partially random, this number will give you consistency 
                copy_x=True, # True is faster, and also fine if we have already centered our data (standardisation)
                n_jobs=None, # how many CPU cores do you want to commit, to run iterations in parallel?
                algorithm='auto') # "auto" chooses "elkan" for dense data and "full" for sparse data
```

cool, what's the output like?

![KMeans Output](images/kmeans_output.png)

That's... not great. This model has a silhouette score of 0.13, which is pretty bad (a score closer to 1 is better, the score is a comparison of variance within a cluster vs distance to nearest cluster).

Let's see if we can beat this with another clusterer.

### Kmodes
By the way, Kmeans has a sister - Kmodes. It's conceptually very similar but it works with boolean (0s and 1s) data. Kmeans isn't as good as it works with euclidian continuous distance, and if all of your data is 0s and 1s then they're essentially an infinite distance apart. Kmodes reperesents all your data points as vectors, and works to minimise the sum of them

```python
clf = KModes(n_clusters = 3, # you define
                 init = 'Huang', # Cao has a consideration of density
                 n_init = 1, # how many iterations do you want to do?
                 n_jobs = -2, # all cpu cores except 1
                 random_state = 42)
```

I'm not fitting it because it's really not suitable for this data, but it's worth knowing about. There's also K-prototypes, which combines Kmeans and Kmodes.

### Affinity Propagation

Affinity Propagation (AP) is both similar and different from KMeans. It's similar in that you are still focused on centroids (though they're now called exemplars), but it's different in that you don't choose how many clusters will be outputted.

Methodology is as follows:

1. All datapoints are computed into a 'similarity matrix', with values indicating how close together those points are
2. Two matrices are made and adjusted in parallel, both consider the similarity matrix above.
    * a 'responsibility matrix' indicates how good an exemplar point k would be for point i, considering the availability of all the other ks
    * an 'availability matrix' indicates how appropriate it is for point i to nominate point k as the exemplar, considering the indications from all the other point i's - it's like all the point i's voting on point k's application to be the exemplar.
3. These two matrices are adjusted iteratively; as the availability matrix changes, the responsibility matrix is updated, which changes the availability matrix again, and so on. The iterations continue until we reach convergence, or we reach max_iterations.
4. Responsibility and availability scores are combined into the 'criterion matrix'. Each point i belongs to the exemplar with the highest availability and responsibility - it is both the best in class candiate (responsibility) and it has won the popular vote (availability)

This sounds like a lot, but it's really just all the point i's casting a vote, then finding out who everyone else voted for, and voting again etc. etc. until everyone's happy with who they're agreeing with, and the candidates with the most votes are the exemplars for their voters.

Cool, so the arguments are as follows:

```python
AP = AffinityPropagation(damping=0.8, # how resistant do we want our matrices to be to change over iterations? 
                                      # (sacrifices clustering precision for a more reliable convergence, 1 is more resistant)
                         max_iter=200, # we'll give up if we haven't reached convergence by then
                         convergence_iter=15, # we have convergence if the number of clusters suggested doesn't change within this
                         copy=True, # copy the dataframe, rather than modifying inplace?
                         preference=None, # pass in a list of equal length to your data. Allows you to set 'favourites',
                                          # which are more likely to be selected as exemplars
                         affinity='euclidean', # 'precomputed' is for if you have already converted X to a similarity matrix
                         verbose=False) # print out diagnostics as you go?
```

![Affinity Propagation Output](images/affinity_propagation.png)

That's worse! This doesn't seem to have done a very good job at clustering. The output is generating a silhouette score of 0.202, which is pretty poor, so KMeans is still the best so far.

### Hierarchical (Agglomerative)

Agglomerative creates a sort of tree as its output, and so it's really good at representing nested clusters.

 It's quite straightforward:

1. Represent every data point as a cluster
2. Merge the two closest clusters together to form a new cluster (and remove the old ones)
3. Repeat step until you only have one cluster left

The really key thing with Agglomerative Clustering is the strategy you use to merge clusters together. Remember that each cluster will (soon) have multiple datapoints in it.

* single (minimum): merges the cluster pair with the smallest distance between their two closest data points
* complete (maximum): merges the cluster pair with the smallest distance between their two most far apart data points
* average: merges the clusters with the smallest average distance between datapoints
* ward: merges the clusters that would result in the lowest total squared distance between all the new cluster's datapoints and the cluster centre. (this metric is conceptually a lot like KMeans)

Agglomerative Clustering merges clusters together until there aren't any more clusters. We can see the order in which clusters will be merged with a dendrogram:

![Agglomerative Clustering Dendrogram](images/AgglomerativeClusteringDendrogram.png)

The dendrogram suggests we could start with 5 clusters, and see how we go.

```python
AG = AgglomerativeClustering(n_clusters=5,
                             affinity='euclidean', # how do we want to assess distance between datapoints?
                             memory=None, # this model is very intensive, do we want to cache it somewhere?
                             connectivity=None, # you can pass a matrix describing the distance between each point and its 
                                                # neighbours. This will make your clustering process much faster.
                             compute_full_tree='auto', # you can stop the clustering prematurely if you've defined n_clusters
                             linkage='ward', # see notes above
                             distance_threshold=None) # how far away do points need to be before we don't group them?
```

Which gives us an output like this:

![Agglomerative Clustering Output](images/agglomerativeclusteringoutput.png)

Interesting. Statistically the model is worse (it has a lower silhouette score), but visually it looks better. It looks like we either need to do more work on the number of clusters, or to try a different evaluation metric.

### DBSCAN

DBScan (Density-Based Spatial Clustering of Applications with Noise) is one I've not played around with a lot, but looks really powerful. It focuses on density within your data, and builds clusters from there. The really important parameters are 'how close do points need to be to be considered in the same neighbourhood', and 'how many points do you need in a neighourhood for it to count as a cluster'. Like Affinity Propagation, you don't choose the number of clusters explicitly yourself.

```python
dbscan = DBSCAN(eps=0.5, # how far apart can points be and still be in the same neighbourhood
                min_samples=5, # how many points does a neighbourhood need to be a cluster
                metric='euclidean', # standard as-the-crow-flies distance metric
                metric_params=None,
                algorithm='auto',
                leaf_size=30,
                p=None,
                n_jobs=None)
```